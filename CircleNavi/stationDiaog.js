import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Button,
  Animated,
  Alert,
  Text,
  Image
} from 'react-native';

export default class StationDialog extends View {

  constructor(props) {
    super(props);
    this.state={
        displayWidth:this.props.displayWidth/2,
        displayHeight:this.props.displayHeight/2,
        stationName:this.props.stationName,
        stationImage:this.props.stationImage,
        viewScale:new Animated.Value(1)
    }

  }

    render() {
        return (
        <Animated.View style={[rootViewStyles.container,{left:(this.state.displayWidth/2)-(150)},{top:(this.state.displayHeight/2)-(150)},{ transform: [{ scale: this.state.viewScale }] }]}>
          <Text style={textStyles.container}>{this.state.stationName}</Text>
          <Image style={imageViewStyles.container} source={this.state.stationImage} />
          <View>
            <Button title="閉じる" onPress={this.closeButtonPressed.bind(this)}/>
          </View>
        </Animated.View>
        );
    }

    changeDialog(){
      // Animated.timing(
      //   this.state.viewScale,
      //   {
      //     toValue: 0,
      //     duration: 300,
      //   }
      // ).start();
      
      // function() {

      // Animated.timing(
      //   this.state.viewScale,
      //   {
      //     toValue: 1,
      //     duration: 300,
      //   }
      // ).start();
    // });
    }

    closeButtonPressed(){
      Animated.timing(
        this.state.viewScale,
        {
          toValue: 0,
          duration: 300,
        }
      ).start();
    }

}

const textStyles = StyleSheet.create({
  container: {
    fontSize:20,
    height:30,
    textAlign:"center"
  }
});

const imageViewStyles = StyleSheet.create({
  container: {
    alignSelf:"center",
    height:220,
    width:250
  }
});

const rootViewStyles = StyleSheet.create({
  container: {
    position: 'absolute',
    width:300,
    height:300,
    backgroundColor:'lightgreen',
    borderColor:'green',
    borderWidth:10,
    opacity:0.9
  }

});
