/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  View,
  ScrollView,
  Image,
  Button,
  Alert,
  TouchableHighlight,
  Animated,
  Text
} from 'react-native';
import StationDialog from './stationDiaog.js'

const Sound = require('react-native-sound');

export default class CircleNavi extends Component {

  constructor(props) {
    super(props);
    this.state = {
      displayWidth: 750,
      displayHeight: 1334,
      imageWidth: 1191,
      imageHeight: 842,
      imageLeft: new Animated.Value(-410),
      imageTop: new Animated.Value(-100),
      imageScale: new Animated.Value(0.42),
      nowState: "阪本",
      dialog:null,
      centerView: true,
      points: [
        { x: 469, y: 126, state: "大阪", sound: require("./assets/sounds/osaka.mp3"), stationImage:require("./assets/images/station/osaka_station.jpg") },
        { x: 381, y: 184, state: "福島", sound: require("./assets/sounds/hukushima.mp3"), stationImage:require("./assets/images/station/fukushima_station.jpg") },
        { x: 316, y: 271, state: "野田", sound: require("./assets/sounds/noda.mp3"), stationImage:require("./assets/images/station/noda_station.jpg") },
        { x: 276, y: 369, state: "西九条", sound: require("./assets/sounds/nishikujo.mp3"), stationImage:require("./assets/images/station/nisikujo_station.jpg") },
        { x: 279, y: 478, state: "弁天町", sound: require("./assets/sounds/bentencho.mp3"), stationImage:require("./assets/images/station/bentencho_station.jpg") },
        { x: 315, y: 580, state: "大正", sound: require("./assets/sounds/taisho.mp3"), stationImage:require("./assets/images/station/taisho_station.jpg") },
        { x: 381, y: 664, state: "芦原橋", sound: require("./assets/sounds/ashiharabashi.mp3"), stationImage:require("./assets/images/station/ashiharabashi_station.jpg") },
        { x: 469, y: 723, state: "今宮", sound: require("./assets/sounds/imamiya.mp3"), stationImage:require("./assets/images/station/imamiya_station.jpg") },
        { x: 574, y: 750, state: "新今宮", sound: require("./assets/sounds/shinimamiya.mp3"), stationImage:require("./assets/images/station/shinimamiya_station.jpg") },
        { x: 682, y: 738, state: "天王寺", sound: require("./assets/sounds/tennnoji.mp3"), stationImage:require("./assets/images/station/tennoji_station.jpg") },
        { x: 780, y: 697, state: "寺田町", sound: require("./assets/sounds/teradatyo.mp3"), stationImage:require("./assets/images/station/teradacho_station.jpg") },
        { x: 858, y: 622, state: "桃谷", sound: require("./assets/sounds/momodani.mp3"), stationImage:require("./assets/images/station/momodani_station.jpg") },
        { x: 909, y: 529, state: "鶴橋", sound: require("./assets/sounds/turuhashi.mp3"), stationImage:require("./assets/images/station/tsuruhashi_station.jpg") },
        { x: 925, y: 421, state: "玉造", sound: require("./assets/sounds/tamatukuri.mp3"), stationImage:require("./assets/images/station/tamatsukuri_station.jpg") },
        { x: 909, y: 315, state: "森ノ宮", sound: require("./assets/sounds/morinomiya.mp3"), stationImage:require("./assets/images/station/morinomiya_station.jpg") },
        { x: 856, y: 223, state: "大阪城公園", sound: require("./assets/sounds/osakajyokoen.mp3"), stationImage:require("./assets/images/station/osakajokoen_station.jpg") },
        { x: 778, y: 148, state: "京橋", sound: require("./assets/sounds/kyobashi.mp3"), stationImage:require("./assets/images/station/kyobashi_station.jpg") },
        { x: 679, y: 105, state: "桜ノ宮", sound: require("./assets/sounds/sakuranomiya.mp3"), stationImage:require("./assets/images/station/sakuranomiya_station.jpg") },
        { x: 573, y: 97, state: "天満", sound: require("./assets/sounds/tenma.mp3"), stationImage:require("./assets/images/station/tennma_station.jpg") }
      ],
      pointIndex: 0
    };

  }

  render() {

    return (
      <View style={rootViewStyles.container}>

        <Animated.View title="??" ref="imageView" style={[contentsViewStyles.container, { left: this.state.imageLeft }, { top: this.state.imageTop }, { transform: [{ scale: this.state.imageScale }] }]}>
          <Image source={require('./assets/images/osakaloop_line-map.png')} />
        </Animated.View>

        <View style={menuViewStyles.container}>
          <TouchableHighlight style={middleButtonStyles.container} onPress={this.middleButtonPressed.bind(this)}>
            <Image source={require('./assets/images/westjr.gif')} />
          </TouchableHighlight>

          <TouchableHighlight onPress={this.leftButtonPressed.bind(this)} style={[leftButtonStyles.container]} title="leftButton" >
            <Image source={require('./assets/images/left_allow.png')} />
          </TouchableHighlight>

          <TouchableHighlight onPress={this.rightButtonPressed.bind(this)} style={rightButtonStyles.container} title="rightButton" >
            <Image source={require('./assets/images/right_allow.png')} />
          </TouchableHighlight>

        </View>

        <StationDialog ref="rere" displayWidth={this.state.displayWidth} displayHeight={this.state.displayHeight} bind={this} />

      </View>
    );
  }

  middleButtonPressed() {
    if (this.state.centerView) {
      Animated.parallel([
        Animated.timing(
          this.state.imageScale,
          {
            toValue: 0.42,
            duration: 300,
          },
        ),
        Animated.timing(
          this.state.imageLeft,
          {
            toValue: -410,
            duration: 300,
          }
        ),
        Animated.timing(
          this.state.imageTop,
          {
            toValue: -100,
            duration: 300,
          }
        )
      ]).start();
    } else {
      this.movePoint();
    }
    this.state.centerView != this.state.centerView;

  }

  leftButtonPressed() {

    if (this.state.pointIndex < this.state.points.length - 1) {
      this.state.pointIndex++;
    } else {
      this.state.pointIndex = 0;
    }
    this.movePoint();
  };

  rightButtonPressed() {
    if (this.state.pointIndex == 0) {
      this.state.pointIndex = this.state.points.length - 1;
    } else {
      this.state.pointIndex--;
    }
    this.movePoint();
  };

  playSound() {

    var stat = this.state.points[this.state.pointIndex];

    var soundObj = new Sound(stat.sound, (error) => {
      if (error) {
        Alert.alert("Sound load fail")
        return;
      }

      soundObj.play((success) => {
        if (success) {
          soundObj.release();
        } else {
          Alert.alert("Sound play fail")
        }
      });

    });
  }

  movePoint() {
    var stat = this.state.points[this.state.pointIndex];

    if (this.state.centerView) {

      Animated.parallel([
        Animated.timing(
          this.state.imageScale,
          {
            toValue: 1,
            duration: 300,
          },
        ),

        Animated.timing(
          this.state.imageLeft,
          {
            toValue: (-stat.x) + (this.state.displayWidth / 4),
            duration: 300,
          }
        ),
        Animated.timing(
          this.state.imageTop,
          {
            toValue: (-stat.y) + (this.state.displayHeight / 4),
            duration: 300,
          }
        )]).start();

    }
    else {
      Animated.parallel([
        Animated.timing(
          this.state.imageLeft,
          {
            toValue: (-stat.x) + (this.state.displayWidth / 4),
            duration: 300,
          }
        ),
        Animated.timing(
          this.state.imageTop,
          {
            toValue: (-stat.y) + (this.state.displayHeight / 4),
            duration: 300,
          }
        )]).start();
    }

    this.setState(
      { nowState: stat.state }
    )
    this.refs.rere.setState(
      {stationName:stat.state}
    );

    this.refs.rere.setState(
      {stationImage:stat.stationImage}
    );
    this.refs.rere.changeDialog();

  }

}

const contentsViewStyles = StyleSheet.create({
  container: {
    marginBottom: 50,
    position: 'absolute',
    left: 0,
    top: 0,
    backgroundColor: '#FF0000'
  }
})

const rootViewStyles = StyleSheet.create({
  container: {
    flex: 1
  }
});

const menuViewStyles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    height: 50,
    width: '100%',
    bottom: 0,
    left: 0,
    position: 'absolute',
    flexDirection: 'row'
  }
});

const leftButtonStyles = StyleSheet.create({
  container: {
    width: 50,
    height: 50,
    position: 'absolute',
    left: 0
  }
});

const middleButtonStyles = StyleSheet.create({
  container: {
    width: '100%',
    height: 50,
    flexDirection: 'column',
    alignItems: 'center'
  }
});

const rightButtonStyles = StyleSheet.create({
  container: {
    width: 50,
    height: 50,
    position: 'absolute',
    right: 0
  }
});

AppRegistry.registerComponent('CircleNavi', () => CircleNavi);
